package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

type Config struct {
	BaseAPI string
	Buttons []Button
}
type Button struct {
	Label  string
	Action string
	Data   url.Values
}

func main() {
	ll := lbllogger(log.Writer())
	log.SetOutput(ll)
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	log.Println("version:", VERSION)

	var config Config = Config{
		BaseAPI: "http://127.0.0.1:24403/api",
		Buttons: []Button{{"Demo1", "demo1", nil}, {"Demo2", "demo2", map[string][]string{"cups": {"2"}}}},
	}

	cf, err := os.Open(configpath())
	if err == nil {
		defer cf.Close()
		cj := json.NewDecoder(cf)
		var emptyConfig Config
		config = emptyConfig
		err = cj.Decode(&config)
		if err != nil {
			log.Println("err decoding, using demo mode defaults", configpath(), ":", err)
		}
		log.Println("loaded settings from", configpath())
	} else {
		log.Println(configpath(), "failed, using demo mode defauts, err: ", err)
	}

	a := app.NewWithID("com.gitlab.rigel314.eventapp")
	w := a.NewWindow("Generic Events")

	darktheme := a.Preferences().BoolWithFallback("theme", false)
	if darktheme {
		a.Settings().SetTheme(theme.DarkTheme())
	} else {
		a.Settings().SetTheme(theme.LightTheme())
	}

	buttons := container.NewGridWithColumns(1)

	for _, v := range config.Buttons {
		v := v
		buttons.Add(newButton(config, v))
	}

	themes := container.New(layout.NewGridLayout(2),
		widget.NewButton("Dark", func() {
			a.Settings().SetTheme(theme.DarkTheme())
			a.Preferences().SetBool("theme", true)
		}),
		widget.NewButton("Light", func() {
			a.Settings().SetTheme(theme.LightTheme())
			a.Preferences().SetBool("theme", false)
		}),
	)
	errlbl := widget.NewLabelWithData(ll.txt)
	errlbl.Wrapping = fyne.TextWrapWord
	errscroll := container.NewVScroll(errlbl)
	buttons.Add(errscroll)
	outer := container.NewBorder(nil, themes, nil, nil, buttons)

	w.SetContent(outer)

	w.Resize(fyne.NewSize(640, 460))

	w.ShowAndRun()
}

func newButton(config Config, b Button) *widget.Button {
	button := widget.NewButton(b.Label, nil)
	button.OnTapped = func() {
		button.Disable()
		log.Println("in button:", b.Label, ", action:", b.Action, ", data:", b.Data)
		go func() { // Don't hang the UI
			defer button.Enable()
			resp, err := http.PostForm(config.BaseAPI+"/"+b.Action, b.Data)
			if err != nil {
				log.Println("error handling button:", b.Label, ", err:", err)
				return
			}
			defer resp.Body.Close()
			if resp.StatusCode != 200 {
				log.Println("error handling button:", b.Label, ", code:", resp.Status)
				return
			}
			bodydec := json.NewDecoder(resp.Body)
			var body map[string]interface{}
			err = bodydec.Decode(&body)
			if err != nil {
				log.Println("error reading response:", b.Label, ", err:", err)
				return
			}
			if _, ok := body["status"].(string); !ok {
				log.Println("invalid response:", b.Label, ", err:", err)
				return
			}
			if body["status"].(string) != "success" {
				log.Println("unsuccessful response:", b.Label, "body:", body)
				return
			}
		}()
	}

	return button
}

type labellogger struct {
	wr  io.Writer
	str string
	txt binding.ExternalString
}

func lbllogger(wr io.Writer) *labellogger {
	var ret labellogger
	ret.wr = wr
	ret.txt = binding.BindString(&ret.str)
	return &ret
}

func (l *labellogger) Write(b []byte) (int, error) {
	n, err := l.wr.Write(b)
	l.str += string(b)
	l.txt.Reload()
	return n, err
}
