// +build android ios

package main

import (
	"log"
	"path/filepath"

	"gitlab.com/rigel314/go-androidutils"
)

func configpath() string {
	defer func() {
		if v := recover(); v != nil {
			log.Println(v)
		}
	}()
	root, err := androidutils.ExternalAppSpecificPath()
	if err != nil {
		log.Println("ExternalAppSpecificPath failed, err: ", err)
		return ""
	}

	return filepath.Join(root, "eventConfig.json")
}
