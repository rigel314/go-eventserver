package main

import (
	"database/sql"
	"embed"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
)

var mem = flag.Bool("mem", false, "use in memory database instead of local mysql server unix socket")

//go:embed www/*
var staticSite embed.FS

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()

	var db *sql.DB
	var err error

	if *mem {
		db, err = sql.Open("sqlite3", "file::memory:?cache=shared")
		if err != nil {
			log.Fatal("failed sqlite3 ram sql.Open:", err)
		}
		cmds = sqliteCmds
	} else {
		db, err = sql.Open("mysql", "user:password@/dbname")
		if err != nil {
			log.Fatal("failed mysql sql.Open:", err)
		}
		db.SetConnMaxLifetime(time.Minute * 3)
		db.SetMaxOpenConns(10)
		db.SetMaxIdleConns(10)
		cmds = mysqlCmds
	}

	// dents, err := staticSite.ReadDir("www")
	// log.Println(err)
	// for _, v := range dents {
	// 	log.Println(v.Name(), v.IsDir())
	// }

	http.Handle("/www/", http.FileServer(http.FS(staticSite)))
	http.Handle("/", http.RedirectHandler("/www/", http.StatusFound))
	http.HandleFunc("/api/", func(rw http.ResponseWriter, r *http.Request) {
		respJson := json.NewEncoder(rw)
		path := strings.TrimPrefix(r.URL.Path, "/api/")
		meth := r.Method
		subpath := strings.SplitN(path, "/", 2)
		// log.Println(meth, path, subpath)
		switch meth {
		case "GET":
			if path == "" { // if you don't ask for a table, give a list of all tables
				rows, err := db.Query(cmds.ListTables)
				if err != nil {
					log.Println(err)
					respJson.Encode(fail(err.Error()))
					return
				}
				defer rows.Close()

				var tables []string
				for rows.Next() {
					var tname string
					err = rows.Scan(&tname)
					if err != nil {
						log.Println(err)
						respJson.Encode(fail(err.Error()))
						return
					}
					tables = append(tables, tname)
				}
				respJson.Encode(tableList(tables))
				return
			}
			if len(subpath) < 2 {
				log.Println("cannot get", path)
				respJson.Encode(fail("cannot get " + path))
				return
			}
			table := subpath[0]
			if !valid(table) {
				log.Println()
				respJson.Encode(fail("invalid table: " + table))
				return
			}
			switch subpath[1] {
			case "":
				log.Println("cannot get", path)
				respJson.Encode(fail("cannot get " + path))
				return
			case "last20":
				rows, err := db.Query("SELECT q.timestamp, q.json_data FROM (SELECT * FROM " + table + " ORDER BY id DESC LIMIT 20) q ORDER BY q.id ASC")
				if err != nil {
					log.Println(err)
					respJson.Encode(fail(err.Error()))
					return
				}
				defer rows.Close()
				var events []Event
				for rows.Next() {
					var event Event
					err = rows.Scan(&event.Time, &event.JsonData)
					if err != nil {
						log.Println(err)
						respJson.Encode(fail(err.Error()))
						return
					}
					events = append(events, event)
				}
				respJson.Encode(last20(events))
				return
			case "avgPeriod":
				rows := db.QueryRow("SELECT AVG(diff) average FROM (SELECT " + cmds.DiffTimestamps + " diff FROM " + table + " t1 INNER JOIN " + table + " t2 ON t2.id = t1.id + 1) diffs")
				var avg float64
				err = rows.Scan(&avg)
				if err != nil {
					log.Println(err)
					respJson.Encode(fail(err.Error()))
					return
				}
				respJson.Encode(avgPeriod(avg))
				return
			}

		case "POST":
			if path == "" {
				log.Println("cannot get", path)
				respJson.Encode(fail("cannot post to " + path))
				return
			}
			if len(subpath) != 1 {
				log.Println("cannot get", path)
				respJson.Encode(fail("cannot get " + path))
				return
			}
			table := subpath[0]
			if !valid(table) {
				log.Println()
				respJson.Encode(fail("invalid table: " + table))
				return
			}

			r.ParseForm()
			var m = make(map[string]string, len(r.PostForm))
			for k := range r.PostForm {
				m[k] = r.PostForm.Get(k)
			}
			jstr, err := json.Marshal(m)
			if err != nil {
				log.Println(err)
				respJson.Encode(fail(err.Error()))
				return
			}

			if *mem {
				var column_decls = "" +
					"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
					"timestamp DATETIME, " +
					"json_data TEXT"

				_, err := db.Exec("CREATE TABLE IF NOT EXISTS " + table + " (" + column_decls + ")")
				if err != nil {
					log.Println(err)
					respJson.Encode(fail(err.Error()))
					return
				}
			}

			_, err = db.Exec("INSERT INTO '"+table+"' (timestamp, json_data)"+" VALUES (?, ?)", time.Now(), jstr)
			if err != nil {
				log.Println(err)
				respJson.Encode(fail(err.Error()))
				return
			}

			respJson.Encode(success)
			return
		}
		respJson.Encode(fail("unhandled failure"))
	})
	log.Println("server up")
	http.ListenAndServe("127.0.0.1:24403", nil)
}

func valid(str string) bool {
	for _, v := range ([]byte)(str) {
		if !(v >= 'a' && v <= 'z') &&
			!(v >= '0' && v <= '9') {
			return false
		}
	}
	return true
}

type BaseResponse struct {
	Status string `json:"status"`
}

var success = BaseResponse{"success"}

type BaseFailure struct {
	BaseResponse
	Reason string `json:"reason"`
}

func fail(reason string) BaseFailure {
	return BaseFailure{BaseResponse{"failure"}, reason}
}

type ListTablesResponse struct {
	BaseResponse
	Tables []string `json:"tables"`
}

func tableList(tables []string) ListTablesResponse {
	return ListTablesResponse{BaseResponse{"success"}, tables}
}

type Last20Response struct {
	BaseResponse
	Events []Event `json:"events"`
}
type Event struct {
	Time     time.Time `json:"time"`
	JsonData string    `json:"json_data"`
}

func last20(Events []Event) Last20Response {
	return Last20Response{BaseResponse{"success"}, Events}
}

type PeriodResponse struct {
	BaseResponse
	Avg float64 `json:"avg"`
}

func avgPeriod(avg float64) PeriodResponse {
	return PeriodResponse{BaseResponse{"success"}, avg}
}
