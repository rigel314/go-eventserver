package main

type sqlCmds struct {
	ListTables     string
	DiffTimestamps string
}

var mysqlCmds = sqlCmds{
	ListTables:     "show tables",
	DiffTimestamps: "TIMESTAMPDIFF(SECOND, t1.timestamp, t2.timestamp)",
}

var sqliteCmds = sqlCmds{
	ListTables:     "select name from sqlite_master where type = 'table' and name not like 'sqlite_%'",
	DiffTimestamps: "((julianday(t2.timestamp) - julianday(t1.timestamp)) * 86400)",
}

var cmds sqlCmds
