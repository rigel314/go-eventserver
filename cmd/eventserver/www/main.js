// on page load
$(function() {
	$.get("/api/")
		.fail(function() {
			$("body").text("no tables (non 2xx response on list tabels)")
		})
		.done(function(data) {
			data = JSON.parse(data)
			if (data.status == null || data.status != "success") {
				$("body").text("no tables (failure response: " + data.reason + ")")
				return
			}
			if (!data.tables || data.tables.length == 0) {
				$("body").text("no tables (actually no tables)")
				return
			}
			let ul = $("<ul></ul>")
			data.tables.forEach(table => {
				let li = $("<li></li>")
				var h2 = $("<h2></h2>").text(table)
				li.append(h2)
				ul.append(li)
				$.get("/api/" + table + "/last20")
					.fail(function() {
						li.append($("<h3></h3>").text("no entries (non 2xx response on last20)"))
					})
					.done(function(data) {
						data = JSON.parse(data)
						if (data.status == null || data.status != "success") {
							li.append($("<h3></h3>").text("no entries (failure response: " + data.reason + ")"))
							return
						}
						if (!data.events || data.events.length == 0) {
							li.append($("<h3></h3>").text("no entries (actually no entries)"))
							return
						}
						var innerul = $("<ul></ul>")
						li.append(innerul)
						li.append(plot(data.events))
						li.append($("<h3></h3>").text("latest: " + data.events[data.events.length - 1].time))
						li.append($("<h3></h3>").text("latest: " + (new Date() - new Date(data.events[data.events.length - 1].time)) + " ms ago"))
						$.get("/api/" + table + "/avgPeriod")
							.fail(function() {
								li.append($("<h3></h3>").text("no avg (non 2xx response on avgPeriod)"))
							})
							.done(function(data) {
								data = JSON.parse(data)
								if (data.status == null || data.status != "success") {
									li.append($("<h3></h3>").text("no avg (failure response: " + data.reason + ")"))
									return
								}
								if (!data.avg) {
									li.append($("<h3></h3>").text("no avg (avg missing)"))
									return
								}
								li.append($("<h3></h3>").text("average: " + 1 / (data.avg / 86400) + " events/day"))
								li.append($("<h3></h3>").text("average: " + (data.avg / 86400) + " days/event"))
							})
					})
			})
			$("body").append(ul)
		})
})

function plot(events) {
	const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")

	var svgSize = new Object()
	svgSize.x = 800
	svgSize.y = 600

	svg.setAttribute("width", svgSize.x.toString())
	svg.setAttribute("height", svgSize.y.toString())

	for (let index = 0; index < 5; index++) {
		var xcoord = svgSize.x / 6 * (index + 1)
		var ycoord = svgSize.y - 100
		svg.appendChild(line(
			xcoord, 0,
			xcoord, ycoord,
			"stroke:rgb(200,200,200);stroke-width:1"
		))
		const txt = document.createElementNS("http://www.w3.org/2000/svg", "text")
		txt.setAttribute("x", xcoord.toString())
		txt.setAttribute("y", (ycoord + 10).toString())
		txt.setAttribute("transform", "rotate(-30 " + xcoord + " " + (ycoord + 10) + ")")
		txt.setAttribute("text-anchor", "end")
		txt.setAttribute("textLength", (svgSize.x / 6 / Math.sin(60 * Math.PI / 180)).toString())
		txt.setAttribute("lengthAdjust", "spacingAndGlyphs")
		txt.innerHTML = "grid " + (index + 1)
		svg.appendChild(txt)
	}

	svg.appendChild(line(
		0, svgSize.y - 100,
		svgSize.x, svgSize.y - 100,
		"stroke:rgb(0,0,0);stroke-width:1"
	))

	var prev = null
	var maxdiff = -Infinity
	var mindiff = Infinity
	var total = 0
	var count = 0
	events.forEach(event => {
		if (prev != null) {
			var diff = new Date(event.time) - new Date(prev.time)
			if (diff < mindiff) {
				mindiff = diff
			}
			if (diff > maxdiff) {
				maxdiff = diff
			}
			total += diff
			count++
		}
		prev = event
	})

	var avg = total / count

	var l = events.length
	var xbase = 3
	var xend = svgSize.x - xbase
	var ybase = 3
	var yend = svgSize.y - 100.5 - ybase
	var prev = null
	var lastdot = null
	events.forEach(event => {
		const circ = document.createElementNS("http://www.w3.org/2000/svg", "circle")
		var xcoord = scale(new Date(event.time), new Date(events[0].time), new Date(events[l - 1].time), xbase, xend)
		var ycoord
		if (prev == null) {
			ycoord = scale(avg, mindiff, maxdiff, ybase, yend)
		} else {
			ycoord = scale(new Date(event.time) - new Date(prev.time), mindiff, maxdiff, ybase, yend)
		}
		circ.setAttribute("cx", xcoord.toString())
		circ.setAttribute("cy", ycoord.toString())
		circ.setAttribute("r", "3")
		circ.setAttribute("fill", "red")
		if (lastdot != null) {
			svg.appendChild(line(
				lastdot.x, lastdot.y,
				xcoord, ycoord,
				"stroke:rgb(255,0,0);stroke-width:1"
			))
		} else {
			lastdot = new Object()
		}
		lastdot.x = xcoord
		lastdot.y = ycoord

		svg.appendChild(circ)
		prev = event
	})

	return svg
}

function scale(nm, n, m, x, y) {
	var tmp = ((nm - n) / (m - n)) // map n->m to 0->1
	return tmp * (y - x) + x // map 0-> to x->y
}

function line(x1, y1, x2, y2, style) {
	const l = document.createElementNS("http://www.w3.org/2000/svg", "line")
	l.setAttribute("x1", x1)
	l.setAttribute("y1", y1)
	l.setAttribute("x2", x2)
	l.setAttribute("y2", y2)
	l.setAttribute("style", style)
	return l
}
