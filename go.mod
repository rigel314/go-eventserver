module gitlab.com/rigel314/go-eventserver

go 1.16

require (
	fyne.io/fyne/v2 v2.0.3
	github.com/go-sql-driver/mysql v1.6.0
	github.com/mattn/go-sqlite3 v1.14.7
	gitlab.com/rigel314/go-androidutils v0.0.2
	golang.org/x/mobile v0.0.0-20201208152944-da85bec010a2 // indirect
)

replace golang.org/x/mobile => github.com/fyne-io/gomobile-bridge v0.0.1

// replace gitlab.com/rigel314/go-androidutils => ../go-androidutils
