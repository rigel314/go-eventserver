SHELL := /bin/bash

ANDROID_NDK_HOME ?= "$(shell pwd)/android-ndk-r21d"

FYNE = $(shell which fyne || echo ~/go/bin/fyne)

APKANALYZER ?= $(shell which apkanalyzer 2> /dev/null)
ifeq (${APKANALYZER},)
APKANALYZER := /mnt/g/Misc/AndroidStudioSDK/tools/bin/apkanalyzer
endif

CI_COMMIT_SHA ?= $(shell git describe --always --dirty --all --long)
CI_COMMIT_TAG ?= ${CI_COMMIT_SHA}
VERSION ?= ${CI_COMMIT_TAG}

appVer = $(shell echo $${CI_COMMIT_TAG:-0.0.1})

standard: app-android-dev app server Makefile

ci: app-android-dev server Makefile

release: app-android server Makefile

windows: app.exe server.exe Makefile

app-android: Makefile
	@cd ./cmd/eventapp && \
	echo -e '// +build android\n\npackage main\n\nvar VERSION string = "'"${VERSION}"'"' > version_real.go && \
	ANDROID_NDK_HOME=${ANDROID_NDK_HOME} ${FYNE} release -os android -icon assets/icon.png -appVersion ${appVer} -appBuild 1 -appID com.gitlab.rigel314.go-eventserver -keyStore /android_upload_keystore.jks -certificate upload -keyStorePass ${KEY_STORE_PASS}
	cp ./cmd/eventapp/*.apk .
app-android-dev: Makefile
	cd ./cmd/eventapp && \
	echo -e '// +build android\n\npackage main\n\nvar VERSION string = "'"${VERSION}"'"' > version_real.go && \
	ANDROID_NDK_HOME=${ANDROID_NDK_HOME} ${FYNE} package -os android -icon assets/icon.png -appVersion 0.0.1 -appID com.gitlab.rigel314.eventapp
	cp ./cmd/eventapp/*.apk .
app: Makefile
	CGO_ENABLED=1 GOOS=linux   GOARCH=amd64 go build -ldflags '-buildid= -w -s' -trimpath ./cmd/eventapp
app.exe: Makefile
	CGO_ENABLED=1 GOOS=windows GOARCH=amd64 go build -ldflags '-buildid= -w -s' -trimpath ./cmd/eventapp

server: Makefile
	CGO_ENABLED=1 GOOS=linux   GOARCH=amd64 go build -ldflags '-buildid= -w -s' -trimpath --tags 'sqlite_json' ./cmd/eventserver
server.exe: Makefile
	CGO_ENABLED=1 GOOS=windows GOARCH=amd64 go build -ldflags '-buildid= -w -s' -trimpath --tags 'sqlite_json' ./cmd/eventserver

extract-manifest:
	$(APKANALYZER) manifest print eventapp.apk

.PHONY: standard app-android app-android-dev extract-manifest
